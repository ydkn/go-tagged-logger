# Tagged logger for go

A simple logger for go that supports tagging.
Inspired by ActiveSupport::TaggedLogging.

## Usage

```go
logger := NewTaggedLoggerStdout()

logger.Printlnf("foo")
// foo

logger.Tagged("TAG", func() {
	logger.Printlnf("bar")
})
// [TAG] bar

logger.EnableTimestamps(true)
logger.Printlnf("foo")
// 2019-07-20T15:27:29Z foo
```

## Notes

It does currently not support running in parallel with goroutines, but you can create a new logger for every goroutine.
