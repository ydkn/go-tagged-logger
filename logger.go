package taggedlogger

import (
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"time"
)

// TaggedLogger logging with nested tags
type TaggedLogger struct {
	out        io.Writer
	lock       sync.Mutex
	timestamps bool
	tags       []string
}

// NewTaggedLogger creates a new new tagged logger
func NewTaggedLogger(out io.Writer) *TaggedLogger {
	return &TaggedLogger{out: out, lock: sync.Mutex{}, timestamps: false, tags: make([]string, 0)}
}

// NewTaggedLoggerStdout creates a new new tagged logger that outputs on stdout
func NewTaggedLoggerStdout() *TaggedLogger {
	return &TaggedLogger{out: os.Stdout, lock: sync.Mutex{}, timestamps: false, tags: make([]string, 0)}
}

// NewTaggedLoggerStderr creates a new new tagged logger that outputs on stderr
func NewTaggedLoggerStderr() *TaggedLogger {
	return &TaggedLogger{out: os.Stderr, lock: sync.Mutex{}, timestamps: false, tags: make([]string, 0)}
}

// EnableTimestamps enable current timestamp as prefix
func (l *TaggedLogger) EnableTimestamps(flag bool) {
	l.timestamps = flag
}

// Printf writes a new formatted log line
func (l *TaggedLogger) Printf(format string, v ...interface{}) {
	_, _ = io.WriteString(l.out, fmt.Sprintf(fmt.Sprintf("%s%s\n", l.tagPrefix(), format), v...))
}

// PrintError writes a new formatted log line
func (l *TaggedLogger) Error(err error) {
	_, _ = io.WriteString(l.out, fmt.Sprintf("%s[ERROR] %s\n", l.tagPrefix(), err.Error()))
}

// Tagged adds tag to stack within fn
func (l *TaggedLogger) Tagged(tag string, fn func()) {
	l.AddTag(tag)
	defer l.RemoveTag()

	fn()
}

// AddTag add a tag
func (l *TaggedLogger) AddTag(tag string) {
	l.lock.Lock()
	defer l.lock.Unlock()

	l.tags = append(l.tags, tag)
}

// RemoveTag removes the upmost tag
func (l *TaggedLogger) RemoveTag() {
	l.lock.Lock()
	defer l.lock.Unlock()

	length := len(l.tags)

	if length == 0 {
		return
	}

	l.tags = l.tags[:length-1]
}

func (l *TaggedLogger) tagPrefix() string {
	prefix := ""

	if l.timestamps {
		prefix = fmt.Sprintf("%s ", time.Now().Format(time.RFC3339))
	}

	if len(l.tags) == 0 {
		return prefix
	}

	tags := []string{}

	for _, tag := range l.tags {
		tags = append(tags, fmt.Sprintf("[%s]", tag))
	}

	return fmt.Sprintf("%s%s ", prefix, strings.Join(tags, " "))
}
