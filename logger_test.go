package taggedlogger

import (
	"testing"

	"bytes"
	"errors"
	"regexp"
	"strings"
)

func bufferTrimedStringAndReset(b *bytes.Buffer) string {
	str := strings.TrimSpace(b.String())

	b.Reset()

	return str
}

func TestWithTimestamp(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)
	logger.EnableTimestamps(true)

	logger.Printf("foobar")

	out := bufferTrimedStringAndReset(buf)

	re := regexp.MustCompile(`^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z(\d{2}:\d{2})? foobar$`)

	if !re.MatchString(out) {
		t.Errorf(`got "%s"; expected "%s"`, out, "<TIMESTAMP> foobar")
	}
}

func TestWithoutTag(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.Printf("foobar")

	out := bufferTrimedStringAndReset(buf)

	if out != "foobar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "foobar")
	}
}

func TestWithTag(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.Tagged("TAG", func() {
		logger.Printf("foobar")
	})

	out := bufferTrimedStringAndReset(buf)

	if out != "[TAG] foobar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] foobar")
	}
}

func TestWithNestedTags(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.Tagged("TAG", func() {
		logger.Tagged("FOO", func() {
			logger.Printf("bar")
		})
	})

	out := bufferTrimedStringAndReset(buf)

	if out != "[TAG] [FOO] bar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] [FOO] bar")
	}
}

func TestMixed(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.Printf("foo")

	out := bufferTrimedStringAndReset(buf)

	if out != "foo" {
		t.Errorf(`got "%s"; expected "%s"`, out, "foo")
	}

	logger.Tagged("TAG", func() {
		logger.Printf("bar")
	})

	out = bufferTrimedStringAndReset(buf)

	if out != "[TAG] bar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] bar")
	}

	logger.Tagged("TAG", func() {
		logger.Tagged("FOO", func() {
			logger.Printf("bar")
		})
	})

	out = bufferTrimedStringAndReset(buf)

	if out != "[TAG] [FOO] bar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] [FOO] bar")
	}

	logger.Printf("test")

	out = bufferTrimedStringAndReset(buf)

	if out != "test" {
		t.Errorf(`got "%s"; expected "%s"`, out, "test")
	}
}

func TestWithManualPushAndPopTags(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.AddTag("TAG")
	logger.Printf("bar")
	out := bufferTrimedStringAndReset(buf)
	if out != "[TAG] bar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] bar")
	}

	logger.AddTag("FOO")
	logger.Printf("bar")
	out = bufferTrimedStringAndReset(buf)
	if out != "[TAG] [FOO] bar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] [FOO] bar")
	}

	logger.RemoveTag()
	logger.Printf("bar")
	out = bufferTrimedStringAndReset(buf)
	if out != "[TAG] bar" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] bar")
	}
}

func TestError(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.Error(errors.New("foo"))

	out := bufferTrimedStringAndReset(buf)

	if out != "[ERROR] foo" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[ERROR] foo")
	}
}

func TestErrorWithTag(t *testing.T) {
	buf := new(bytes.Buffer)
	logger := NewTaggedLogger(buf)

	logger.Tagged("TAG", func() {
		logger.Error(errors.New("foo"))
	})

	out := bufferTrimedStringAndReset(buf)

	if out != "[TAG] [ERROR] foo" {
		t.Errorf(`got "%s"; expected "%s"`, out, "[TAG] [ERROR] foo")
	}
}
